# OpenML dataset: Colon

https://www.openml.org/d/45087

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Colon dataset**

**Authors**: U. Alon, N. Barkai, D. Notterman, K. Gish, S. Ybarra, D. Mack, A. Levine

**Please cite**: ([URL](https://www.pnas.org/doi/abs/10.1073/pnas.96.12.6745)): U. Alon, N. Barkai, D. Notterman, K. Gish, S. Ybarra, D. Mack, A. Levine, Broad patterns of gene expression revealed by clustering analysis of tumor and normal colon tissues probed by oligonucleotide arrays, Proc. Nat. Acad. Sci. 96 (12) (1999) 6745-6750

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45087) of an [OpenML dataset](https://www.openml.org/d/45087). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45087/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45087/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45087/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

